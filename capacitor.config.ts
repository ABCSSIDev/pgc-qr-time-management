import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.abcssi.pgc.ph',
  appName: 'Bayanihan Time Management',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
