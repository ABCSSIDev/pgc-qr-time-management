<h1>Bayanihan Time Management (Kiosk Type)</h1>
<p>This app often include a <b>time clock or android application used to track an employee's work hours.</p>
<h2>Core Features:</h2>
<ul>
  <li>Offline Mode Time-in and out working hours</li>
  <li>QR Code Scanner</li>
  <li>Capture Current Location when Time-in</li>
  <li>Auto sync timelogs when detect network</li>
</ul>
<h1>Setup to your Local Development Environment</h1>
<h2>Requirements:</h2>
<ul>
  <li>Ionic Framework <code>v6</code> <a href="https://ionicframework.com/">Click this link for more information.</a></li>
  <li>Android Studio with SDK(Software Development Kit)</li>
</ul>
<h2>Instructions:</h2>
<ul>
  <li>Run <code>npm install</code></li>
  <li>Use COMMAND PROMPT for performing command lines in the specific project directory.</li>
  <li>run <code>npm ionic build</code> to generate www directory.</li>
  <li>run <code>ionic capacitor sync android</code>.</li>
  <li>run <code>ionic capacitor open android</code> to automatically open yung project to Android Studio.</li>
  <li>Lastly run your app via Android Studio. Enjoy! :)</li>
</ul>

