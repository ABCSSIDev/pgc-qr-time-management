
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap, map } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Observable, from } from 'rxjs';
import { EnvService } from './env.service';
import { NetworkService, ConnectionStatus } from './network.service';

const API_STORAGE_KEY = 'specialkey';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  location: any;
  isTimeSetup = false;
  timeSetup:any;
 
    constructor(
      private http: HttpClient,
      private env: EnvService,
      private storage: NativeStorage,
      private networkService: NetworkService
    ) { 
    }

    getUsers(forceRefresh: boolean = false): Observable<any> {
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
          // Return the cached data from Storage
          return from(this.getLocalData('users'));
        } else {
          // Return real API data and store it locally
          return this.http.get(`${this.env.APP_URL}/app/users`).pipe(
            map(res => res),
            tap(res => {
                this.setLocalData('users', res);
            })
          )
        }
      }
     
      // Save result of API requests
      private setLocalData(key, data) {
        this.storage.setItem(`${API_STORAGE_KEY}-${key}`, data);
      }
     
      // Get cached API result
      private getLocalData(key) {
        return this.storage.getItem(`${API_STORAGE_KEY}-${key}`);
      }
}
