import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from, of, forkJoin } from 'rxjs';
import { switchMap, finalize } from 'rxjs/operators';
import { ToastController, LoadingController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

const STORAGE_REQ_KEY = 'storedreq';

interface StoredRequest {
    url: string,
    type: string,
    data: any,
    time: number,
    id: string
}

@Injectable({
    providedIn: 'root'
})

export class OfflineManagerService {

    loadingData: any;

    constructor(
        private storage: NativeStorage,
        private http: HttpClient,
        private toastController: ToastController,
        private loadingCtrl: LoadingController
    ) { }
 
    checkForEvents(): Observable<any> {
        
        return from(this.storage.getItem(STORAGE_REQ_KEY)).pipe(
        switchMap(storedOperations => {
            let storedObj = JSON.parse(storedOperations);
            if (storedObj.length > 0) {
                this.loading();
                return this.sendRequests(storedObj).pipe(
                    finalize(() => {
                    this.loadingData.dismiss();
                    let toast = this.toastController.create({
                        message: `Local data succesfully synced to API!`,
                        duration: 3000,
                        position: 'bottom'
                    });
                    toast.then(toast => toast.present());
        
                    this.storage.remove(STORAGE_REQ_KEY);
                    })
                );
            } else {
                alert('no local events to sync');
                return of(false);
            }
        })
        )
    }

    async loading () {
        this.loadingData = await this.loadingCtrl.create({message: 'Loading....'});
        this.loadingData.present();
    }
 
    storeRequest(url, type, data) {
        let toast = this.toastController.create({
        message: `Your data is stored locally because you seem to be offline.`,
        duration: 3000,
        position: 'bottom'
        });
        toast.then(toast => toast.present());
    
        let action: StoredRequest = {
        url: url,
        type: type,
        data: data,
        time: new Date().getTime(),
        id: Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
        };
    
        return this.storage.getItem(STORAGE_REQ_KEY).then(storedOperations => {
        let storedObj = JSON.parse(storedOperations);
    
        if (storedObj) {
            storedObj.push(action);
        } else {
            storedObj = [action];
        }
        // Save old & new local transactions back to Storage
        return this.storage.setItem(STORAGE_REQ_KEY, JSON.stringify(storedObj));
        },(error) => {
            this.storage.setItem(STORAGE_REQ_KEY, JSON.stringify([action]));
        });
    }
 
    sendRequests(operations: StoredRequest[]) {
        let obs = [];
    
        for (let op of operations) {
        let oneObs = this.http.post(op.url, op.data);
        obs.push(oneObs);
        }
    
        // Send out all local events and return once they are finished
        return forkJoin(obs);
    }
}