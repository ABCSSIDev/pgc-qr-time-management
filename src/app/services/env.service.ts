import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  APP_URL = 'https://pgc.com.ph/time_management/public/api';
  //APP_URL = 'http://staging.abcsystems.com.ph/mmri-web-service/public/api';

  constructor() { }
}
