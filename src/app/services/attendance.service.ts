import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap, catchError } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { from } from 'rxjs';

import { EnvService } from './env.service';
import { Attendance } from '../models/attendance';
import { NetworkService, ConnectionStatus } from './network.service';
import { OfflineManagerService } from './offline-manager.service';

const API_STORAGE_KEY = 'specialkey';
@Injectable({
  providedIn: 'root'
})

export class AttendanceService {
  location: any;
  isTimeSetup = false;
  timeSetup:any;

  attendanceInfo: Attendance;
  
 
    constructor(
      private http: HttpClient,
      private env: EnvService,
      private storage: NativeStorage,
      private networkService: NetworkService,
      private offlineManager: OfflineManagerService,
    ) { 
      this.location = {
        latitude: null,
        longitude: null
      }
      
      this.storage.getItem('location').then(data => {
        this.location['latitude'] = data['latitude'];
        this.location['longitude'] = data['longitude'];
      });
    }

    qrEmployeeTimeIn(employee_no: String, data) {

      data['longitude'] = this.location['latitude'];
      data['latitude'] = this.location['longitude'];

      if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
        return from(this.offlineManager.storeRequest(`${this.env.APP_URL}/app/store/attendance`, 'POST', data));
      } else {

        return this.http.post<Attendance>(`${this.env.APP_URL}/app/timelog/qr/time-in/` + employee_no, {})
        .pipe(
          tap(attendance => {
              return attendance;
          })
        )
      }
        
    }

    qrEmployeeTimeOut(employee_no: String, data) {

        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {

          return from(this.offlineManager.storeRequest(`${this.env.APP_URL}/app/store/attendance`, 'POST', data));
        } else {

          return this.http.post<Attendance>(`${this.env.APP_URL}/app/timelog/qr/time-out/` + employee_no, {})
          .pipe(
            tap(attendance => {
                return attendance;
            })
          )
        }
    }

    getTimeSetup() {
        return this.storage.getItem('timeSetup').then(
        data => {
            this.timeSetup = data;
            if(this.timeSetup != null) {
            this.isTimeSetup=true;
            } else {
            this.isTimeSetup=false;
            }
        },
        error => {
            this.timeSetup = null;
            this.isTimeSetup=false;
        }
        );
    }

    // getUsers(employee_no: String, date: Date, type: String) {
    //   this.userService.getUsers(true).subscribe(res => {
    //     res.forEach(user => {
    //         if (user['employee_no'] == employee_no) {
    //           this.attendanceInfo.name = user['name'];
    //           this.attendanceInfo.date = date.toDateString();
    //           this.attendanceInfo.image = user['avatar'];
    //           this.attendanceInfo.designation = user['designation'] + '(' + user['department'] + ')';

    //           if (type == 'time-in') {
    //             this.attendanceInfo.time_in = 'Started At' + date.getHours() + ':' + date.getMinutes();
    //           }
              
    //           if (type == 'time-out') {
    //             this.attendanceInfo.time_out = date.getHours() + ':' + date.getMinutes();
    //           }

    //           this.attendanceInfo.hours_worked = 'N/A';

    //           return this.attendanceInfo;
    //         }
    //     });
    //   });
    // }
}
