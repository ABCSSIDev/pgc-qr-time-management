import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';

   
@Injectable({
    providedIn: 'root'
})

export class ClockService {

    today: any;
    hours: any;
    minutes: any;
    seconds: any;
    date: any;
    current_time: any;
    users = [];

    constructor(
        private storage: NativeStorage
    ) { }

    getTimeClock() {
        this.getOfflineTimeClock().then( data => {
            this.today = data;
            if (this.today != null || this.today != undefined) {
              this.offlineTimeClock(new Date(data));   
              if (this.isDateBeforeToday(new Date(data))) {
                this.today = new Date();
                this.offlineTimeClock(new Date(this.today));
                this.storage.setItem('dateToday', this.today);
              }
            } else {
              this.today = new Date();
              this.offlineTimeClock(new Date(this.today));
              this.storage.setItem('dateToday', this.today);
            }
          },() => {
            this.today = new Date();
            this.offlineTimeClock(this.today);
            this.storage.setItem('dateToday', this.today);
          }
        );
    }


    getOfflineTimeClock() {
        return this.storage.getItem('dateToday');
    }
    
    offlineTimeClock(today) {
        this.hours = today.getHours(); // 0 - 23
        this.minutes = today.getMinutes(); // 0 - 59
        this.seconds = today.getSeconds(); // 0 - 59
        this.date = today.toDateString();
    }
    
    refreshTime()
    {
        this.seconds = this.seconds + 1;
    
        if (this.seconds == 60) {
          this.seconds = 0;
          this.minutes = this.minutes + 1;
        }
    
        if (this.minutes == 60 && this.seconds == 0) {
          this.hours = this.hours + 1;
          this.minutes = 0;
        }
        
        if (this.hours == 24 && this.minutes == 0 && this.seconds == 0) {
          this.hours = 0;
          this.minutes = 0;
          this.seconds = 0;
    
          let cloneDate = new Date(this.today.valueOf());
          cloneDate.setDate(cloneDate.getDate() + 1);
          this.date = cloneDate.toDateString();
        }
        
        this.current_time = new Date( this.date + ' ' + this.hours + ':' + this.minutes + ':' + this.seconds);

        return this.current_time;
    }
    
    
    isDateBeforeToday(date) {
        return (date < new Date());
    }
}
