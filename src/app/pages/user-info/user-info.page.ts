import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { Attendance } from 'src/app/models/attendance';
import { AlertService } from 'src/app/services/alert.service';
import { AttendanceService } from 'src/app/services/attendance.service';
import { NetworkService, ConnectionStatus } from 'src/app/services/network.service';
import { UserService } from 'src/app/services/user.service';
import { QrScanInfoPage } from '../qr-scan-info/qr-scan-info.page';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.page.html',
  styleUrls: ['./user-info.page.scss'],
})
export class UserInfoPage implements OnInit {
  @Input() employeeNo: String;
  @Input() dateStamp: any;
  attendanceInfo: Attendance;
  current_time: any;

  constructor(
    private attendanceService: AttendanceService,
    private modalController: ModalController,
    private loadingCtrl: LoadingController,
    private alertService: AlertService,
    private userService: UserService,
    private networkService: NetworkService
  ) {
   }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    if (this.attendanceService.timeSetup == 'time_in') {
      this.timein();
    } else if (this.attendanceService.timeSetup == 'time_out') {
      this.endShift();
    }
  }

  intervalQRScan() {
    setTimeout(() => {
      this.qrScanner();
    }, 4000)
  }

  async timein() {
    let data = {
      date: this.dateStamp,
      type: 'time-in',
      employee_no: this.employeeNo
    }

    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.qrEmployeeTimeIn(this.employeeNo, data).subscribe(
      attendance => {
        loading.dismiss();
        this.alertService.presentToast('Successfully Time-in!');

        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
          this.userService.getUsers(true).subscribe(res => {
            res.forEach(user => {
                if (user['employee_no'] == this.employeeNo) {
                  user['date'] = data.date.toDateString();
                  if (data.type == 'time-in') {
                    user['time_in'] = 'Started At' + data.date.getHours() + ':' + data.date.getMinutes();
                  }
                  
                  if (data.type == 'time-out') {
                    user['time_out'] = data.date.getHours() + ':' + data.date.getMinutes();
                  }
  
                  this.attendanceInfo = user;
                }
            });
          });
        } else {
          this.attendanceInfo = attendance;
        }
      
        this.intervalQRScan();
      }, error => {
        loading.dismiss();
        this.intervalQRScan();
        //console.log(error)
        alert(JSON.stringify(error));
      }
    );
  }

  async endShift() {
    let data = {
      date: this.dateStamp,
      type: 'time-out',
      employee_no: this.employeeNo
    }

    const loading = await this.loadingCtrl.create({message: 'Loading....'});
    loading.present();
    this.attendanceService.qrEmployeeTimeOut(this.employeeNo, data).subscribe(
      attendance => {
        loading.dismiss()
        this.alertService.presentToast('Successfully End your Shift!');
        
        if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
          this.userService.getUsers(true).subscribe(res => {
            res.forEach(user => {
                if (user['employee_no'] == this.employeeNo) {
                  user['date'] = data.date.toDateString();
                  if (data.type == 'time-in') {
                    user['time_in'] = 'Started At' + data.date.getHours() + ':' + data.date.getMinutes();
                  }
                  
                  if (data.type == 'time-out') {
                    user['time_out'] = data.date.getHours() + ':' + data.date.getMinutes();
                  }
  
                  this.attendanceInfo = user;
                }
            });
          });
        } else {
          this.attendanceInfo = attendance;
        }
        
        this.intervalQRScan();
      }, error => {
        loading.dismiss();
        this.intervalQRScan();
        alert(JSON.stringify(error));
      }
    );
  }

  async qrScanner() {
    this.modalController.dismiss();
    console.log('QR Scanner');
    const reportModal = await this.modalController.create({
      component: QrScanInfoPage,
    });
    return await reportModal.present();
  }


}
