import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QrScanInfoPage } from './qr-scan-info.page';

const routes: Routes = [
  {
    path: '',
    component: QrScanInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrScanInfoPageRoutingModule {}
