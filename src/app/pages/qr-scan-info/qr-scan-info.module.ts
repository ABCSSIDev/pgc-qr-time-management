import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrScanInfoPageRoutingModule } from './qr-scan-info-routing.module';

import { QrScanInfoPage } from './qr-scan-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrScanInfoPageRoutingModule
  ],
  declarations: [QrScanInfoPage]
})
export class QrScanInfoPageModule {}
