import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, MenuController, ModalController, Platform } from '@ionic/angular';
import { Attendance } from 'src/app/models/attendance';
import { AlertService } from 'src/app/services/alert.service';
import { AttendanceService } from 'src/app/services/attendance.service';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { UserInfoPage } from '../user-info/user-info.page';
import { CurrentLocationService } from 'src/app/services/current-location.service';
import { ClockService } from 'src/app/services/clock.service';

@Component({
  selector: 'app-qr-scan-info',
  templateUrl: './qr-scan-info.page.html',
  styleUrls: ['./qr-scan-info.page.scss'],
})
export class QrScanInfoPage implements OnInit {
  @Input() timeSetup: String;
  employeeNo : any;
  attendanceInfo: Attendance;
  scanSubscription: any;
  employee_no: String;
  current_time: any;

  constructor(
    private platform: Platform,
    private menu: MenuController,
    private qrScanner: QRScanner,
    private modalController: ModalController,
    private locationService: CurrentLocationService,
    private attendanceService: AttendanceService,
    private clock: ClockService
  ) { 
    this.menu.enable(false);
    this.platform.backButton.subscribeWithPriority(0, () => {
      document.getElementsByTagName("body")[0].style.opacity = "1";
      this.scanSubscription.unsubscribe();
    })

    this.clock.getTimeClock();
    
    setInterval(() => { 
        this.current_time = this.clock.refreshTime(); 
    }, 1000);
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.locationService.askToTurnOnGPS();
    this.qrscanner();
  }

  qrscanner() {
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          this.qrScanner.show();
          document.getElementsByTagName("body")[0].style.opacity = '0';
          this.scanSubscription = this.qrScanner.scan()
            .subscribe((text: String) => {
              this.attendanceService.getTimeSetup();
              this.userInfoPage(text);
            }
          );
        } else {
          console.error('Permission Denied', status);
        }
      })
      .catch((e: any) => {
        console.error('Error', e);
      });
  }

  async userInfoPage(employeeNo: String) {
    console.log('QR Scanner');
    this.modalController.dismiss();
    const reportModal = await this.modalController.create({
      component: UserInfoPage,
      componentProps: { 
        employeeNo: employeeNo,
        dateStamp: this.current_time,
        timeSetup: this.timeSetup
      }
    });
    document.getElementsByTagName("body")[0].style.opacity = '1';
    this.qrScanner.hide();
    this.scanSubscription.unsubscribe();
    return await reportModal.present();
  }

}
