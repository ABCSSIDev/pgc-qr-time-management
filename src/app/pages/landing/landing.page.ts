import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { MenuController, ModalController, Platform } from '@ionic/angular';
import { QrScanInfoPage } from '../qr-scan-info/qr-scan-info.page';
import { Attendance } from 'src/app/models/attendance';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  attendanceInfo: Attendance;
  scanSubscription: any;
  current_time: any;
  users = [];

  constructor(
    private modalController: ModalController,
    private menu: MenuController,
    private storage: NativeStorage,
    private platform: Platform,
    private userService: UserService,
  ) {

    this.menu.enable(false);
  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.loadData(true);
    });
  }

  loadData(refresh = false, refresher?) {
    this.userService.getUsers(refresh).subscribe(res => {
      console.log(res);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }
  
  async QRScannerView(timeSetup: String) {
    this.storage.remove('timeSetup');
    this.storage.setItem('timeSetup', timeSetup)
    console.log('QR Scanner');
    const reportModal = await this.modalController.create({
      component: QrScanInfoPage,
      componentProps: { 
        timeSetup: timeSetup
      }

    });
    return await reportModal.present();
  }
}
